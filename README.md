# Widget Bundler

Bundle html, javascript, stylesheet and images into widget for iBook.

## Installation

Add this line to your application's Gemfile:

    gem 'widget_bundler'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install widget_bundler

## Usage

    widget_bundle = WidgetBundle.new(
      images: images,  # array, gem assumes "#{Rails.root}/public" for images (paperclip default path) 
      javascripts: javascripts, # array of urls
      stylesheets: stylesheets, # array of urls
      haml: haml, # url string
      thumbnail: thumnbail # url string
    )

  The HAML file *must* be present, the other inputs are optional.

     path_to_zip_file = widget_bundle.package(display_name, bundle_identifier, bundle_name, root_path)

  You can then package this bundle by provoding the display name, bundle identifier, bundle name and root path (optional).

  This will zip this folder with a generated Info.plist file.
