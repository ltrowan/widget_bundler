#
# Class for packaging set of html, javascript, image and stylesheet files into a widget for an iBook
# => accepts an array of images, stylesheets and javascripts as full @paths
# => images should be paperclip .url method @path (gem assumes "#{Rails.root}/public" @path for images)
# => acceps main haml file as a string
#
require 'active_model'
require 'plist'
require 'uglifier'
require 'sass'

class WidgetBundle

  # Rails mixins
  include ActiveModel::Validations

  # images, stylesheets and javascripts as file @path arrays
  # haml file as string, full@path to main.haml
  # thumbnail as full@path to image, will be renamed to default.png
  attr_accessor :images, :stylesheets, :javascripts, :haml, :thumbnail, 
    :display_name, :bundle_identifier, :bundle_name, :root_path, :js_extension
  validates :haml, presence: true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end

    js_extension ||= "js"
  end

  # Package the widget contents into a folder for download
  # 	root_path: the rootpath of the files within the zip folder
  # 	zip - can pass an open zip directory to add members too
  def package(zip=nil)
    return false unless valid?

    @root_path = root_path
    @title = bundle_name
    @title = @title.gsub(" ", "-")
    @path = File.expand_path "#{Rails.root}/tmp/#{@root_path}/#{@title}.wdgt/"
    FileUtils.rm_rf @path
    FileUtils.mkdir_p @path

    # make requred directories
    images_dir = @path
    unless Dir.exist?("#{@path}/assets/")
      css_js_dir = FileUtils.mkdir("#{@path}/assets/")[0]
    end

    # copy images
    # 	TODO: tried doing this all with FileUtils.cp_r but couldn't get it to work properly, had to use this block instead
    if images
      images.each do |i|
        file = "#{Rails.root}/public#{i}".split("?")[0]
        dir = "#{images_dir}/#{File.dirname(i)}".gsub('//', '/')
        FileUtils.mkdir_p(dir) unless Dir.exist?(dir)
        FileUtils.cp_r file, dir
      end
    end

    # minify stylesheets
    minify_stylesheets(stylesheets)

    # minify javascripts
    minify_javascripts(javascripts)

    # create Info.plist
    File.open("#{@path}/Info.plist", "w") { |file| file.write(plist_content(display_name, bundle_identifier, bundle_name)) }

    # create main.html
    # compiled_haml = `haml #{haml}`
    haml.each do |filename, content|
      File.open("#{@path}/#{filename}.html", "w") { |file| file.write( content ) }
    end

    # copy thumbnail to ./default.png
    FileUtils.cp("#{Rails.root}/public#{thumbnail.split('?')[0]}", "#{@path}/default.png") if thumbnail

    # compress the *.wdgt folder and return the full @path to the zip archive
    @zip = zip
    compress	
  end

  protected

  # Minify stylesheets into one file
  #
  def minify_stylesheets(stylesheets)
    out = ''
    stylesheets.each do |css|
      out << Sass::Engine.new(File.open("#{Rails.root}/public/#{css}").read, syntax: :scss, style: :compressed).render
    end
    out.gsub!(/(?<=['"])\/assets/, 'assets')
    out.gsub!(/(?<=['"])\/system/, 'system')

    # write the main.css file
    File.open("#{@path}/assets/main.css", 'w'){|file| file.write(out); file.close }
  end

  # Minify javascripts into one file
  #
  def minify_javascripts(javascripts)
    out = ''
    javascripts.each do |js|
      out << Uglifier.compile(File.read("#{Rails.root}/public/#{js}"), output: {ascii_only: false})
    end
    out.gsub!(/(?<=['"])\/assets/, 'assets')
    out.gsub!(/(?<=['"])\/system/, 'system')


    # write the main.js file
    File.open("#{@path}/assets/main.#{js_extension}", 'w'){|file| file.write(out); file.close }
  end

  # Compresses the widget folder to a zip for downloading.  Zip file will be created within the *.wdgt folder.
  # 	@path: full @path to *.wdgt folder
  def compress
    gem 'rubyzip'
    require 'zip/zip'
    require 'zip/zipfilesystem'

    @path.sub!(%r[/$],'')
    archive = "#{@path}.zip"

    if @zip.nil?
      FileUtils.rm archive, :force=>true if @zip.nil?
      @zip = Zip::ZipFile.open(archive, 'w')
    end

    Dir["#{@path}/**/**"].reject{|f|f==archive}.each do |file|
      begin
        @zip.add("#{@root_path}/#{@title}/#{file.sub(@path+'/','')}".gsub(/^\//, ""),file)
      rescue Exception => e
        Rails.logger.error e
      end
    end
    @zip.close
    archive
  end

  # Generate plist for widget
  # 
  def plist_content(display_name, bundle_identifier, bundle_name)
    plist = {
      CFBundleDevelopmentRegion: 'English',
      CFBundleDisplayName: display_name,
      CFBundleIdentifier: bundle_identifier,
      CFBundleName: bundle_name,
      CFBundleShortVersionString: '1.0',
      CFBundleVersion: '1.0',
      CloseBoxInsetX: 15,
      CloseBoxInsetY: 15,
      Height: 768,
      MainHTML: 'main.html',
      Width: 1024
    }.to_plist
  end
end
