require "spec_helper"

describe WidgetBundle do

	describe "invalid bundle" do
		widget_bundle = WidgetBundle.new
		widget_bundle.valid?.should == false
	end

	describe "#initialize" do
		before(:each) do
			create_widget_bundle
		end
		it "should be valid" do
			@widget_bundle.valid?.should == true
		end
		it "should set the images correctly" do
			@widget_bundle.images.should == @images
		end

		it "should set the haml correctly" do
			@widget_bundle.haml.should == @haml
		end

		it "should set the stylesheets correctly" do
			@widget_bundle.stylesheets.should == @stylesheets
		end

		it "should set the javascripts correctly" do
			@widget_bundle.javascripts.should == @javascripts
		end
	end

	describe "#package" do
		before(:each) do
			create_widget_bundle
			@dir = File.dirname(@widget_bundle.package('test', 'test', 'test', "tmp"))
		end

		after(:each) do
			FileUtils.rm_rf @dirname + '/tmp'
		end

		it "should create the path" do
			File.exist?("tmp").should == true
		end

		it "should copy the images" do
			Dir.glob("#{@dir}/images/*").should == ["#{@dir}/images/hackers.jpg", "#{@dir}/images/no.png"]
		end

		it "should generate the Info.plist" do
			File.exist?("#{@dir}/Info.plist").should == true
		end

		it "should compile the haml file to main.html" do
			File.exist?("#{@dir}/main.html").should == true
		end
	end

	describe "#compress" do
		before(:each) do
			create_widget_bundle
			@dir = @widget_bundle.package('test', 'test', 'test', 'tmp')
		end
		after(:each) do
			FileUtils.rm_rf @dirname + '/tmp'
		end
		it "should create the zip file" do
			File.exist?(@dir).should == true
		end
	end

	describe "#minify_stylesheets" do
		before(:each) do
			create_widget_bundle
			@dir = File.dirname(@widget_bundle.package('test', 'test', 'test', 'tmp'))
		end
		after(:each) do
			FileUtils.rm_rf @dirname + '/tmp'
		end
		it "should minify stylsheets into assets/main.css" do
			File.exist?("#{@dir}/assets/main.css").should == true
		end
	end

	describe "#minify_javascripts" do
		before(:each) do
			create_widget_bundle
			@dir = File.dirname(@widget_bundle.package('test', 'test', 'test', 'tmp'))
		end
		after(:each) do
			FileUtils.rm_rf @dirname + '/tmp'
		end
		it "should minify javascripts into assets/main.js" do
			File.exist?("#{@dir}/assets/main.js").should == true
		end
	end
end

def create_widget_bundle
	@dirname = File.expand_path(Dir.pwd)
	@images = ["/images/no.png", "/images/hackers.jpg"]
	@javascripts = ["#{@dirname}/spec/test_files/javascripts/test_file1.js", "#{@dirname}/spec/test_files/javascripts/test_file2.js"]
	@stylesheets = ["#{@dirname}/spec/test_files/stylesheets/test_file1.css", "#{@dirname}/spec/test_files/stylesheets/test_file2.css"]
	@haml = "#{@dirname}/spec/test_files/main.haml"
	@widget_bundle = WidgetBundle.new(
		images: @images,
		javascripts: @javascripts,
		stylesheets: @stylesheets,
		haml: @haml
	)
end