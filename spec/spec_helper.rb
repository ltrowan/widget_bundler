require 'widget_bundler'

class WidgetBundle
	module Rails
		def self.root
			"#{File.expand_path(Dir.pwd)}/spec/test_files"
		end
	end
end