Gem::Specification.new do |s|
	s.name			= 'widget_bundler'
	s.version		= '0.0.11'
	s.date			= '2013-04-15'
	s.summary		= %q{Packages files into widget for iBook}
	s.description	= %q{Creates widget for iBook using html, javascript, stylesheet and images files}
	s.authors		= ["Nick Franken", "Brandon Fulk", "Ross Harrison"]
  s.email			= ["nf@the42.com", "bf@the42.com", "rh@the42.com"]
	s.files			= ["lib/widget_bundler.rb", "lib/app/models/widget_bundle.rb"]
	s.add_dependency 'haml', '>= 4.0.0'
	s.add_dependency 'rubyzip'
	s.add_dependency 'plist'
	s.add_dependency 'uglifier'
	s.add_dependency 'sass-rails'
	s.add_development_dependency 'rake'
	s.add_development_dependency 'rspec'
end
